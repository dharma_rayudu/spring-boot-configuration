package com.example.springbootconfiguration;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springbootconfiguration.config.DBSettings;

@RestController
public class ResourceControll {
	
	//@Value is going to inject value from properties file.
	
	@Autowired
	private Environment env;
	
	@Value("${my.greeting}")
	private String greetingMessage;
	
	@Value("${my.description}")
	private String getAppDescription;
	
	@Value("${my.test:Default Test}")
	private String testMessage;
	
	@Value("${my.list.values}")
	private List<String> listValues;
	
	@Value("${test.name}")
	private String testName;
	/*@Value("#{${dbValues}}")
	private Map<String, String> dbValues;*/
	
	@Autowired
	private DBSettings dbSettings;
	
	@GetMapping("/greeting")
	public String getGreeting() {
		return greetingMessage + listValues;
	}
	
	@GetMapping("/description")
	public String getDescription() {
		return getAppDescription;
	}

	@GetMapping("/test")
	public String test() {
		return testMessage+" " +testName;
	}
	
	@GetMapping("/db")
	public String getDBDetails() {
		return dbSettings.getConnection() + dbSettings.getHost() + dbSettings.getPort();
	}
	
	@GetMapping("/envDetails")
	public String getEnv() {
		return env.toString();
	}
}
